import {combineReducers, createStore} from "redux";
import headerReducer from "./headerReducer";
import authReducer from "./authReducer";
import catalogReducer from "./catalogReducer";
import profileItemsReducer from "./ProfileItemsReducer";


let reducers = combineReducers({
    auth: authReducer,
    header: headerReducer,
    catalog: catalogReducer,
    profileItems: profileItemsReducer
})

let store = createStore(reducers);
export default store;