let initialState = {
    userId: null,
    isAuth: false,
    isAdmin: false
};

let authReducer =  (state = initialState, action) => {
    switch (action.type) {
        case "SET-ID": {
            let newState = {
                ...state,
                userId: action.id
            }
            return newState
        }
        case "SET-AUTH": {
            let newState = {
                ...state,
                isAuth: action.flag
            }
            return newState;
        }
        case "SET-ADMIN" : {
            let newState = {
                ...state,
                isAdmin: action.isAdmin
            }
            return newState;
        }
        default: return state;
    }
}

export default authReducer;

export const setUserIdActionCreator = (id) => {
    return {type: 'SET-ID', id}
}

export const setAuthActionCreator = (flag) => {
    return {type: "SET-AUTH", flag}
}

export const setAdminActionCreatore = (isAdmin) => {
    return {type: "SET-ADMIN", isAdmin}
}