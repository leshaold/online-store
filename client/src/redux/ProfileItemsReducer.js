let initialState = {
    profileInfo: {},
    ordersInfo: [],
    cartItems: [],
    favItems: [],
    searchItems: [],
    zakazInfo: [],
    isDeleting: ''
};

let profileItemsReducer =  (state = initialState, action) => {
    switch (action.type) {
        case "SET-FAV-ITEMS": {
            let newState = {
                ...state,
                favItems: action.data
            }
            return newState;
        }
        case "SET-ZAKAZ-INFO": {
            let newState = {
                ...state,
                zakazInfo: action.data
            }
            return newState;
        }
        case "SET-SEARCH-ITEMS": {
            let newState = {
                ...state,
                searchItems: action.searchItems
            }
            return newState;
        }
        case "SET-ORDERS-INFO" : {
            let newState = {
                ...state,
                ordersInfo: state.ordersInfo.length ? state.ordersInfo.concat(action.ordersInfo) : action.ordersInfo
            }
            return newState
        }
        case "SET-RESET-ORDERS" : {
            let newState = {
                ...state,
                ordersInfo: []
            }
            return newState;
        }
        case "SET-PROFILE-INFO": {
            let newState = {
                ...state,
                profileInfo: action.profileInfo
            }
            return newState;
        }
        case "SET-CART-ITEMS": {
            let newState = {
                ...state,
                cartItems: action.data
            }
            return newState;
        }
        case "SET-DELETING": {
            let newState = {
                ...state,
                isDeleting: action.id
            }
            return newState;
        }
        default: return state;
    }
}

export default profileItemsReducer;

export const setFavItemsActionCreator = (data) => {
    return {type: 'SET-FAV-ITEMS', data}
}

export const setCartItemsActionCreator = (data) => {
    return {type: 'SET-CART-ITEMS', data}
}

export const setDeleteItemActionCreator = (id) => {
    return {type: 'SET-DELETING', id}
}

export const setProfileInfoActionCreator = (profileInfo) => {
    return {type: 'SET-PROFILE-INFO', profileInfo}
}

export const setZakazInfoActionCreator = (data) => {
    return {type: 'SET-ZAKAZ-INFO', data}
}

export const setSearchItemsActionCreator = (searchItems) => {
    return {type: 'SET-SEARCH-ITEMS', searchItems}
}

export const setOrdersInfoActionCreator = (ordersInfo) => {
    return {type: 'SET-ORDERS-INFO', ordersInfo}
}

export const setResetOrdersActionCreator = () => {
    return {type: "SET-RESET-ORDERS"}
}