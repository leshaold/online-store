import axios from "axios";
import jwt_decode from "jwt-decode";
import {setAuthActionCreator, setUserIdActionCreator} from "../redux/authReducer";

export const API_URL = 'http://185.7.84.163:8080'

const API = axios.create({
    baseURL: API_URL
})

API.interceptors.request.use((config) => {
    config.headers.Authorization = `Bearer ${localStorage.getItem('token')}`
    return config;
})

API.interceptors.response.use((config) => {
    return config;
}, async (error) => {
    const originalRequest = error.config;
    if (error.response.status === 403 && error.config && !error.config._isRetry) {
        originalRequest._isRetry = true;
        const refreshToken = localStorage.getItem('refreshToken');
        const response = await axios.post('http://185.7.84.163:8080/api/auth/token', {refreshToken})
            .then(response => {
                try {
                    localStorage.setItem('token', response.data.accessToken)
                    const token = response.data.accessToken;
                    return API.request(originalRequest);
                } catch (e) {
                    console.log("Not authorized");
                }
            });
    }
    throw error;
});

export default API;