import React from "react";
import classes from './Detail.module.css';
import Button from "../../Button/Button";
import src from "../../../img/11255768-1.jpg";

const Detail = (props) => {
    let style = {
    }
    return (
       <div className={classes.item_container}>
           <div className={classes.item_image} style={{backgroundImage: `url(${props.config.img ? `data:image/jpeg;base64, ${props.item.img}` : }}`}}/>
            <div className={classes.item_title}>{props.config.name}</div>
           <div className={classes.item_price_container}>
              <div className={classes.item_price}>{props.config.price}₽</div>
                    <Button width="60" height="22" padding='4px' caption="Купить" link='#'/>
           </div>
       </div>
    )
}

export default Detail;