import React from "react";
import classes from './Item.module.css';
import {Link, Navigate} from "react-router-dom";
import Button from "../../../Button/Button";

const Item = (props) => {
    let style = {
        background: `url(data:image/jpeg;base64,${props.config.img})`,
        width: '90%',
        margin: '5px auto 0 auto',
        height: '215px',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    }
    return (
        <Link className={classes.item_wrapper} to={`/item/${props.config.id}`}>
           <div className={classes.item_container}>
               <div className={classes.item_image} style={style}>
               </div>
                <div className={classes.item_title}>{props.config.name}</div>
               <div className={classes.item_price_container}>
                  <div className={classes.item_price}>{props.config.price}₽</div>
                   <div className={classes.item_button}>
                       <Button width="60" height="22" padding='4px' caption="Купить" link='#'/>
                   </div>
               </div>
           </div>
        </Link>
    )
}

export default Item;