import React, {useEffect, useState} from "react";
import classes from './Profile.module.css';
import logo from '../../img/PersonalPhoto.png'
import visa from '../../img/visa_PNG37.png'
import home from '../../img/home.png'
import img from '../../img/11255768-1.jpg';
import AuthForm from "../Authetification/AuthForm/AuthForm";
import {Navigate, useNavigate} from "react-router-dom";
import MyOrders from "./MyOrders/MyOrders";
import MyCards from "./MyCards/MyCards";
import MyAddresses from "./MyAddresses/MyAddresses";
import {useDispatch, useSelector} from "react-redux";
import API from "../../api/api";
import {
    setOrdersInfoActionCreator,
    setProfileInfoActionCreator,
    setResetOrdersActionCreator
} from "../../redux/ProfileItemsReducer";
import OrderInfo from "./OrderInfo/OrderInfo";
import {setAuthActionCreator} from "../../redux/authReducer";
import {setLoadingActionCreator} from "../../redux/catalogReducer";
import Preloader from "../Loader/Loader";

const Profile = (props) => {
    const profileItems = useSelector((state => state.profileItems));
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const isAuth = useSelector(state => state.auth.isAuth);
    let isLoading = useSelector(state => state.catalog.isLoading);
    let [currentPage, setCurrentPage] = useState(0);
    let [hasMore, setHasMore] = useState(false);
    let [totalPage, setTotalPage] = useState(0);
    useEffect(() => {
        if (isAuth) {
            dispatch(setLoadingActionCreator(true))
            API.get('/api/client/profile/').then(res => {
                dispatch(setProfileInfoActionCreator(res.data));
                dispatch(setLoadingActionCreator(false))
            })
        }
        return () => {
            dispatch(setResetOrdersActionCreator());
        }
    }, [])
    useEffect(() => {
        if (isAuth) {
            API.get(`/api/client/profile/orders?page=${currentPage}`).then(res => {
                dispatch(setOrdersInfoActionCreator(res.data.orders));
                setTotalPage(res.data.totalPage);
                setHasMore(currentPage < res.data.totalPage);
            })
        }
    }, [currentPage])
    const logout = () => {
        localStorage.removeItem('token');
        dispatch(setAuthActionCreator(false));
        navigate('/signup');
    }
    if (isLoading) return  <Preloader/>
    if (!isAuth) return  <Navigate to='/signup' replace/>
    return (
         <div className={classes.lk_container}>
             <div>
                 <div className={classes.lk_info_container}>
                     <div>
                         <img className={classes.lk_profile_avatar} src={logo} />
                     </div>
                     <div>
                             <h1 className={classes.lk_title}>{profileItems.profileInfo.name}</h1>
                             <span>{profileItems.profileInfo.email}</span>
                            <div onClick={() => logout()}>Выйти</div>
                     </div>
                 </div>
             </div>
             <div class={classes.lk_block}>
                 <h1 className={classes.lk_block_title}>Мои заказы</h1>
                 {profileItems.zakazInfo.length ? <OrderInfo source={profileItems.zakazInfo}/> : <div> <div className={classes.lk_orders_area}>
                     { profileItems.ordersInfo.length ? profileItems.ordersInfo?.map(order => {
                         return <MyOrders source={order}/>
                     }) : <span>Здесь ничего нет</span>
                     }
                 </div> {hasMore ? <div onClick={() => setCurrentPage(++currentPage)} className={classes.lk_more_button}>Показать еще</div> : '' } </div>}
             </div>
             <div className={classes.lk_block}>
                 <h1 className={classes.lk_block_title}>Способы оплаты</h1>
                 <MyCards/>
             </div>
             <div className={classes.lk_block_addresses}>
                 <h1 className={classes.lk_block_title}>Мои адреса</h1>
                 <MyAddresses source = {profileItems.profileInfo.address}/>
             </div>
         </div>
    )
}

export default Profile;