import React, {useState} from "react";
import classes from './Admin.module.css';
import {Link} from "react-router-dom";
import MyOrders from "../Profile/MyOrders/MyOrders";

const Admin = (props) => {
    let [currentPage, setCurrentPage] = useState(0);
    let source = {
        id: 1,
        status: "Доставлено",
        date: "2002-22-22",
        delivery: "Самовывоз"
    }
    return (
          <div>
              <div>
                  <div className={classes.admin_block}>
                      <h1 className={classes.admin_block_title}>Все заказы</h1>
                      <div className={classes.admin_blocks_orders}>
                          <div className={classes.admin_block_order}>
                              <MyOrders source={source}/>
                          </div>
                          <div className={classes.admin_block_order}>
                              <MyOrders source={source}/>
                          </div>
                          <div className={classes.admin_block_order}>
                              <MyOrders source={source}/>
                          </div>
                          <div className={classes.admin_block_order}>
                              <MyOrders source={source}/>
                          </div>
                          <div className={classes.admin_block_order}>
                              <MyOrders source={source}/>
                          </div>
                      </div>
                      {true? <div onClick={() => setCurrentPage(++currentPage)} className={classes.lk_more_button}>Показать еще</div> : '' }
                  </div>
              </div>

          </div>
    )
}

export default Admin;