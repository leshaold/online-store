import React, {createRef, useState} from "react";
import classes from './Item.module.css';
import {Link} from "react-router-dom";
import image from '../../../../img/11255768-1.jpg';
import Button from "../../../Button/Button";
import favActive from '../../../../img/favorites-red.png';
import favInactive from '../../../../img/favorites.svg';
import deleteIcon from '../../../../img/delete.png';
import {useDispatch, useSelector} from "react-redux";
import {setDeleteItemActionCreator, setZakazInfoActionCreator} from "../../../../redux/ProfileItemsReducer";
import API from "../../../../api/api";
import src from '../../../../img/11255768-1.jpg'
import OrderInfo from "../../../Profile/OrderInfo/OrderInfo";
import {useNavigate} from "react-router";
import {resetSearchActionCreator} from "../../../../redux/headerReducer";

const Item = (props) => {
    let [isAdd, setIsAdd] = useState(props.item.hasFavorites);
    let [flag, setFlag] = useState(false);
    let isAuth = useSelector(state => state.auth.isAuth);
    let profileItems = useSelector(state => state.profileItems);
    let navigate = useNavigate();
    const dispatch = useDispatch();
    let changeItem = (event) => {
        event.preventDefault();
        dispatch(setDeleteItemActionCreator(props.item.idVariant));
    }
    let buyItem = (event, item) => {
        event.preventDefault();
        dispatch(setZakazInfoActionCreator([{name: item.name, img: item.img, price: item.price, size: item.size, idVariant: item.idVariant}]));
    }
    let hideSearch = () => {
        if (props.page === "Поиск") {
            dispatch(resetSearchActionCreator())
        }
    }
    let addToCart = (event) => {
        event.preventDefault();
        if (props.page === "Поиск") {
            dispatch(resetSearchActionCreator())
        }
        navigate(`/item/${props.item.id}`)
    }
    let addToFav = (event) => {
        event.preventDefault();
        if (isAuth) {
            if (!isAdd) {
                API.post(`/api/client/favorites?productId=${props.item.id}`).then(res => setIsAdd(true))
            }
            else {
                API.delete(`/api/client/favorites`, {data: [props.item.id]}).then(res => console.log("ok"))
            }
        }
        else {
            navigate('/signup')
        }
    }
    let deleteFromFav = (event) => {
        event.preventDefault();
        dispatch(setDeleteItemActionCreator(props.item.id));
        setIsAdd(false);
    }
    return (
        <div>
            {profileItems?.zakazInfo?.length? <OrderInfo source={profileItems.zakazInfo} action="Оформление заказа"/> :
                <Link to={`/item/${props.item.id}`} className={classes.item_wrapper} onClick={() => hideSearch()}>
                    <div className={classes.item_container} >
                        <Link to='/' className={classes.item_image_container}>
                            <img className={classes.item_image} src={props.item.img? `data:image/jpeg;base64, ${props.item.img}` : src}/>
                        </Link>
                        <div className={classes.item_info}>
                            <div className={classes.item_title}> {props.item.name} </div>
                            <div className={classes.right_info}>
                                <div className={classes.right_info_top}>
                                    <div className={classes.item_price}> {props.item.price.value}{props.item.price.currency}</div>
                                </div>
                                <div className={classes.right_info_bottom}>
                                    <div className={classes.item_status} >
                                        {props.page === "Корзина"? <img className={classes.item_statusIcon} src={deleteIcon} onClick={(event) => changeItem(event)}/> :
                                            props.page === "Избранное" || isAdd ? <div onClick={(event) => deleteFromFav(event)}> <img className={classes.item_statusIcon} src={favActive}/> </div> :
                                                <div onClick={(e) => addToFav(e)}> <img className={classes.item_statusIcon} src={favInactive} /> </div>}
                                    </div>
                                    {props.page === 'Корзина' ? <div onClick={(event => buyItem(event, props.item))}> <Button link = '#' width='59' height='21' padding="4px" radius="5px" caption="Купить"/> </div>  : ''}
                                    {props.page !== 'Корзина' ? <div className={classes.item_add} onClick={(event) => addToCart(event)}> {flag ? 'Добавлено' : 'Посмотреть'} </div>: ''
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </Link>
            }
        </div>
    )
}

export default Item;