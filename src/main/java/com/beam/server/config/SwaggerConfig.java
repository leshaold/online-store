package com.beam.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .apiInfo(getApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.beam.server.rest"))
                .paths(PathSelectors.any())
                .build();
    }
    private ApiInfo getApiInfo() {
        Contact contact = new Contact("GitLab project link", "https://gitlab.com/leshaold/online-store", null);
        return new ApiInfoBuilder()
                .title("Documentation for online store \"Beam\"")
                .description("Information about REST API controllers")
                .version("0.2")
                .contact(contact)
                .build();
    }
}
