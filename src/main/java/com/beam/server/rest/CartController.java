package com.beam.server.rest;

import com.beam.server.domain.JwtAuthentication;
import com.beam.server.model.CartEntity;
import com.beam.server.model.ClientEntity;
import com.beam.server.model.ProductVariantsEntity;
import com.beam.server.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/client")
@Api(tags = "/api/client")
@CrossOrigin
public class CartController {

    private ObjectMapper mapper = new ObjectMapper();
    @Autowired
    private CartService cartService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private ProductVariantsService productVariantsService;
    @Autowired
    private AuthService authService;

    @ApiOperation(value = "Обновление товара в корзине", notes = "Обновление товара в корзине")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученых данных"),
            @ApiResponse(code = 404, message = "Данные не найдены в БД"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    @PreAuthorize("hasAuthority('CLIENT')")
    @RequestMapping(value = "/cart", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> updateCart(@RequestParam int productVariantId,
                                                 @RequestParam int qty) {
        final JwtAuthentication authInfo = authService.getAuthInfo();
        long clientId = Long.valueOf(authInfo.getId());
        CartEntity cartEntity = cartService.findByClientAndProduct(clientId, productVariantId);
        if (cartEntity == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        if (qty <= 0)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        ClientEntity client;
        ProductVariantsEntity productVariants;
        try {
            Optional<ClientEntity> clientEntityOptional = clientService.getClientById(clientId);
            client = clientEntityOptional.get();
            Optional<ProductVariantsEntity> productVariantsEntityOptional = productVariantsService.findById(productVariantId);
            productVariants = productVariantsEntityOptional.get();
        } catch (NoSuchElementException e) {
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        cartEntity.setClient(client);
        cartEntity.setProduct(productVariants);
        cartEntity.setQty(qty);
        cartService.updateCart(cartEntity);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление товара из корзины", notes = "Удаление товара из корзины")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученых данных"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    @PreAuthorize("hasAuthority('CLIENT')")
    @RequestMapping(value = "/cart", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayNode> deleteFromCart(@RequestBody List<Long> productVariantList) {
        final JwtAuthentication authInfo = authService.getAuthInfo();
        long clientId = Long.valueOf(authInfo.getId());
        if (clientId < 1)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        for (long productVariantId : productVariantList) {
            if (productVariantId < 1)
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            cartService.deleteByClientAndProduct(clientId, productVariantId);
        }
        return getCart(clientId);
    }

    @ApiOperation(value = "Добавление товара в корзину", notes = "Добавление товара в корзину")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученых данных"),
            @ApiResponse(code = 404, message = "Данные не найдены в БД"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    @PreAuthorize("hasAuthority('CLIENT')")
    @RequestMapping(value = "/cart", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> addToCart(@RequestBody HashMap<String, Integer> body) {
        final JwtAuthentication authInfo = authService.getAuthInfo();
        long clientId = Long.valueOf(authInfo.getId());
        CartEntity cartEntity = cartService.findByClientAndProduct(clientId, body.get("productVariantId"));
        if (cartEntity != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (body.get("count") <= 0)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        Optional<ClientEntity> clientEntityOptional = clientService.getClientById(clientId);
        ClientEntity client = clientEntityOptional.get();
        if (client == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        Optional<ProductVariantsEntity> productVariantsEntityOptional = productVariantsService.findById(body.get("productVariantId"));
        ProductVariantsEntity productVariants = productVariantsEntityOptional.get();
        if (productVariants == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        CartEntity cart = new CartEntity();
        cart.setClient(client);
        cart.setProduct(productVariants);
        cart.setQty(body.get("count"));
        cartService.addProductToCart(cart);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Просмотр корзины", notes = "Просмотр корзины")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    @PreAuthorize("hasAuthority('CLIENT')")
    @RequestMapping(value = "/cart", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayNode> getCart() {
        final JwtAuthentication authInfo = authService.getAuthInfo();
        long clientId = Long.valueOf(authInfo.getId());

        return getCart(clientId);
    }

    private ResponseEntity<ArrayNode> getCart(long clientId) {
        List<CartEntity> cart = cartService.findAllByIdClient(clientId);
        ArrayNode products = mapper.createArrayNode();
        for (CartEntity entity : cart) {
            products.add(Utils.getJsonProductInfo(mapper, entity.getProduct(), entity.getQty()));
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }
}
