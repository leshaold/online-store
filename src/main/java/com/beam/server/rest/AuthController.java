package com.beam.server.rest;

import com.beam.server.domain.JwtRequest;
import com.beam.server.domain.JwtResponse;
import com.beam.server.domain.RefreshJwtRequest;
import com.beam.server.service.AuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.message.AuthException;
@Slf4j
@RestController
@RequestMapping("api/auth")
@RequiredArgsConstructor
@Api(tags = "/api/auth")
@CrossOrigin
public class AuthController {
    private final AuthService authService;

    @ApiOperation(value = "Авторизация пользователя", notes = "Авторизация пользователя")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Запрос выполнен успешно. Пользователь авторизован"),
    })
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JwtResponse> login(@RequestBody JwtRequest authRequest) {
        log.info("login() controller is executed");
        final JwtResponse token;
        try {
            token = authService.login(authRequest);
        } catch (AuthException e) {
            log.warn(e.getMessage());
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return ResponseEntity.ok(token);
    }

    @ApiOperation(value = "Получение access токена", notes = "Получение access токена")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    @RequestMapping(value = "/token", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JwtResponse> getNewAccessToken(@RequestBody RefreshJwtRequest request) {
        log.info("getNewAccessToken() controller is executed");
        final JwtResponse token;
        try {
            token = authService.getAccessToken(request.getRefreshToken());
        } catch (AuthException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            //throw new RuntimeException(e);
        }
        return ResponseEntity.ok(token);
    }

    @ApiOperation(value = "Получение access и refresh токенов", notes = "Получение access и refresh токенов")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    @RequestMapping(value = "/refresh", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JwtResponse> getNewRefreshToken(@RequestBody RefreshJwtRequest request) {
        log.info("getNewRefreshToken() controller is executed");
        final JwtResponse token;
        try {
            token = authService.refresh(request.getRefreshToken());
        } catch (AuthException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            //throw new RuntimeException(e);
        }
        return ResponseEntity.ok(token);
    }
}
