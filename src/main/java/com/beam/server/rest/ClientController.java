package com.beam.server.rest;

import com.beam.server.domain.JwtAuthentication;
import com.beam.server.domain.JwtRequest;
import com.beam.server.domain.JwtResponse;
import com.beam.server.model.*;
import com.beam.server.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.message.AuthException;
import java.util.*;

@Slf4j
@RestController
@RequestMapping("/api/client")
@Api(tags = "/api/client")
@CrossOrigin
public class ClientController {

    private ObjectMapper mapper = new ObjectMapper();
    @Autowired
    private ClientService clientService;
    @Autowired
    private AuthService authService;
    @Autowired
    private OrderService orderService;

    @ApiOperation(value = "Регистрация нового пользователя", notes = "Регистрация нового пользователя")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученном JSON"),
            @ApiResponse(code = 409, message = "Пользователь с таким email уже существует"),
            @ApiResponse(code = 201, message = "Запрос выполнен успешно. Новый пользователь создан"),
    })
    @RequestMapping(value = "/reg", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JwtResponse> reg(@RequestBody ClientEntity client) {
        log.info("reg() controller is executed");
        if (client == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        int count = clientService.countAllByEmail(client.getEmail());
        if (count != 0)
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        JwtRequest authRequest = new JwtRequest();
        authRequest.setEmail(client.getEmail());
        authRequest.setPassword(client.getPassword());

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        client.setPassword(encoder.encode(client.getPassword()));
        clientService.createClient(client);

        final JwtResponse token;
        try {
            token = authService.login(authRequest);
        } catch (AuthException e) {
            throw new RuntimeException(e);
        }
        return new ResponseEntity<>(token, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Получение списка заказов пользователя", notes = "Получение списка заказов пользователя")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученном ID"),
            @ApiResponse(code = 201, message = "Запрос выполнен успешно")
    })
    @PreAuthorize("hasAuthority('CLIENT')")
    @RequestMapping(value = "/profile/orders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> getOrders(@RequestParam("page") Integer page) {
        final JwtAuthentication authInfo = authService.getAuthInfo();
        long id = Long.valueOf(authInfo.getId());
        Pageable pageable = PageRequest.of(page, 3);
        Page<OrderEntity> pageOrders = orderService.findAllOrderByClientId(id, pageable);
        List<OrderEntity> orderEntities = new ArrayList<>();
        if (orderEntities != null)
            orderEntities = pageOrders.stream().toList();
        if (orderEntities.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        ArrayNode orders = mapper.createArrayNode();
        ObjectNode json = mapper.createObjectNode();
        for (OrderEntity entity : orderEntities) {
            ObjectNode item = mapper.createObjectNode();
            item.put("id", entity.getId());
            item.put("date", String.valueOf(entity.getDate()));
            item.put("status", entity.getStatus().getName());
            item.put("delivery", entity.getDelivery().getName());
            item.put("deliveryPrice", entity.getDelivery().getPrice());
            orders.add(item);
        }
        json.set("orders", orders);
        json.put("totalPage", pageOrders.getTotalPages() - 1);
        return new ResponseEntity<>(json, HttpStatus.OK);
    }

    @ApiOperation(value = "Получение информации для профиля пользователя", notes = "Получение информации для профиля пользователя")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученном ID"),
            @ApiResponse(code = 201, message = "Запрос выполнен успешно")
    })
    @PreAuthorize("hasAuthority('CLIENT')")
    @RequestMapping(value = "/profile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> getProfile() {
        final JwtAuthentication authInfo = authService.getAuthInfo();
        long id = Long.valueOf(authInfo.getId());
        Optional<ClientEntity> clientOptional = clientService.getClientById(id);
        ClientEntity clientEntity = clientOptional.get();
        if (clientEntity == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        ObjectNode profile = mapper.createObjectNode();
        profile.put("name", clientEntity.getFullName());
        profile.put("email", clientEntity.getEmail());
        profile.put("address", clientEntity.getAddress());

        return new ResponseEntity<>(profile, HttpStatus.OK);
    }

    @ApiOperation(value = "Изменение адреса пользователя", notes = "Изменение адреса пользователя")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученном ID"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно")
    })
    @PreAuthorize("hasAuthority('CLIENT')")
    @RequestMapping(value = "/address", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, String>> changeAddress (@RequestBody HashMap<String, String> addressJson) {
        final JwtAuthentication authInfo = authService.getAuthInfo();
        long clientId = Long.valueOf(authInfo.getId());
        Optional<ClientEntity> clientEntityOptional = clientService.getClientById(clientId);
        ClientEntity client = clientEntityOptional.get();
        if (client == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        String address = addressJson.get("address");
        client.setAddress(address);
        clientService.setAddress(client);
        return new ResponseEntity<>(addressJson, HttpStatus.OK);
    }
}
