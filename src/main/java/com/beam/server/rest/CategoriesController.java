package com.beam.server.rest;

import com.beam.server.domain.Variable;
import com.beam.server.model.ProductTypeEntity;
import com.beam.server.service.OrderBodyService;
import com.beam.server.service.ProductTypeService;
import com.beam.server.service.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@Slf4j
@RestController
@RequestMapping("/api/categories")
@Api(tags = "/api/categories")
public class CategoriesController {
    @Autowired
    private ProductTypeService productTypeService;

    @Autowired
    private OrderBodyService orderBodyService;

    @ApiOperation(value = "Получение 5 случайных типов (категорий товаров)", notes = "Получение 5 случайных типов (категорий товаров")
    @ApiResponses({
            @ApiResponse(code = 404, message = "Объекты не найдены в БД"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    @RequestMapping(value = "/random", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin
    public ResponseEntity<ArrayNode> getPopularProductType() {
        log.info("getPopularProductType() controller is executed");
        ObjectMapper mapper = new ObjectMapper();
        Random r = new Random();
        ArrayNode responseArray = mapper.createArrayNode();
        ArrayList<Integer> trash = new ArrayList<>();
        Set<Integer> set = new HashSet();
        int countProductType = (int) productTypeService.getCountProductType();
        if (countProductType == 0)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        while (responseArray.size() < 5) {
            set.clear();
            while (set.size() < 5) {
                Integer randomNum = r.nextInt(1, countProductType);
                if (!trash.contains(randomNum))
                    set.add(randomNum);
            }
            for (Integer id : set) {
                trash.add(id);
                Optional<ProductTypeEntity> productTypeEntityOptional = productTypeService.getProductTypeEntityById(id);
                ProductTypeEntity productTypeEntity = productTypeEntityOptional.get();
                if (productTypeEntity.getGender().getName().equals(Variable.FOR_MAN) ||
                productTypeEntity.getGender().getName().equals(Variable.FOR_WOMAN)) {
                    ObjectNode responseNode = mapper.createObjectNode();
                    String name = productTypeEntity.getName().getName();
                    if (productTypeEntity.getGender().getName().equals(Variable.FOR_MAN)) {
                        responseNode.put("name", name + " мужские");
                    } else {
                        responseNode.put("name", name + " женские");
                    }
                    responseNode.put("img", Utils.getImgBase64(productTypeEntity.getImg()));
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(productTypeEntity.getGender().getHref());
                    stringBuilder.append(productTypeEntity.getClazz().getHref());
                    stringBuilder.append(productTypeEntity.getName().getHref());
                    responseNode.put("href", stringBuilder.toString());
                    responseArray.add(responseNode);
                    if (responseArray.size() == 5)
                        return new ResponseEntity<>(responseArray, HttpStatus.OK);
                }
            }
        }
        return new ResponseEntity<>(responseArray, HttpStatus.OK);
    }
    @ApiOperation(value = "Получение всех типов (категорий товаров)", notes = "Получение всех типов (категорий товаров")
    @ApiResponses({
            @ApiResponse(code = 404, message = "Объект не найден в БД"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin
    public ResponseEntity<ArrayNode> getAllTypeProduct() {
        log.info("getAllTypeProduct() controller is executed");
        List<ProductTypeEntity> allProductType = this.productTypeService.getAllProductType();
        if (allProductType == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode baseJsonObject = mapper.createArrayNode();

        //TODO По возможности избавиться от этого ужасного дублирования
        ObjectNode forMan = mapper.createObjectNode().put("title", Variable.FOR_MAN).putNull("href");
        ArrayNode classListMan = mapper.createArrayNode();
        ArrayNode itemsShoesForMan = mapper.createArrayNode();
        ArrayNode itemsClothesForMan = mapper.createArrayNode();
        ObjectNode shoesForMan = mapper.createObjectNode()
                .put("name", Variable.SHOES)
                .putNull("href");
        ObjectNode clothesForMan = mapper.createObjectNode()
                .put("name", Variable.CLOTHES)
                .putNull("href");

        ObjectNode forWoman = mapper.createObjectNode().put("title", Variable.FOR_WOMAN).putNull("href");
        ArrayNode classListWoman = mapper.createArrayNode();
        ArrayNode itemsShoesForWoman = mapper.createArrayNode();
        ArrayNode itemsClothesForWoman = mapper.createArrayNode();
        ObjectNode shoesForWoman = mapper.createObjectNode()
                .put("name", Variable.SHOES)
                .putNull("href");
        ObjectNode clothesForWoman = mapper.createObjectNode()
                .put("name", Variable.CLOTHES)
                .putNull("href");

        ObjectNode forBoy = mapper.createObjectNode().put("title", Variable.FOR_BOY).putNull("href");
        ArrayNode classListBoy = mapper.createArrayNode();
        ArrayNode itemsShoesForBoy = mapper.createArrayNode();
        ArrayNode itemsClothesForBoy = mapper.createArrayNode();
        ObjectNode shoesForBoy = mapper.createObjectNode()
                .put("name", Variable.SHOES)
                .putNull("href");
        ObjectNode clothesForBoy = mapper.createObjectNode()
                .put("name", Variable.CLOTHES)
                .putNull("href");

        ObjectNode forGirl = mapper.createObjectNode().put("title", Variable.FOR_GIRL).putNull("href");
        ArrayNode classListGirl = mapper.createArrayNode();
        ArrayNode itemsShoesForGirl = mapper.createArrayNode();
        ArrayNode itemsClothesForGirl = mapper.createArrayNode();
        ObjectNode shoesForGirl = mapper.createObjectNode()
                .put("name", Variable.SHOES)
                .putNull("href");
        ObjectNode clothesForGirl = mapper.createObjectNode()
                .put("name", Variable.CLOTHES)
                .putNull("href");

        for (ProductTypeEntity entity : allProductType) {
            switch (entity.getGender().getName()) {
                case Variable.FOR_MAN ->
                    addObjectToJson(
                            entity,
                            forMan,
                            clothesForMan,
                            shoesForMan,
                            itemsClothesForMan,
                            itemsShoesForMan);
                case Variable.FOR_WOMAN ->
                    addObjectToJson(
                            entity,
                            forWoman,
                            clothesForWoman,
                            shoesForWoman,
                            itemsClothesForWoman,
                            itemsShoesForWoman);

                case Variable.FOR_BOY ->
                    addObjectToJson(
                            entity,
                            forBoy,
                            clothesForBoy,
                            shoesForBoy,
                            itemsClothesForBoy,
                            itemsShoesForBoy);
                case Variable.FOR_GIRL ->
                    addObjectToJson(
                            entity,
                            forGirl,
                            clothesForGirl,
                            shoesForGirl,
                            itemsClothesForGirl,
                            itemsShoesForGirl);
            }
        }

        buildJson(baseJsonObject,
                classListMan,
                clothesForMan,
                shoesForMan,
                itemsShoesForMan,
                itemsClothesForMan,
                forMan);

        buildJson(baseJsonObject,
                classListWoman,
                clothesForWoman,
                shoesForWoman,
                itemsShoesForWoman,
                itemsClothesForWoman,
                forWoman);

        buildJson(baseJsonObject,
                classListBoy,
                clothesForBoy,
                shoesForBoy,
                itemsShoesForBoy,
                itemsClothesForBoy,
                forBoy);

        buildJson(baseJsonObject,
                classListGirl,
                clothesForGirl,
                shoesForGirl,
                itemsShoesForGirl,
                itemsClothesForGirl,
                forGirl);

        return new ResponseEntity<>(baseJsonObject, HttpStatus.OK);
    }
    private void addObjectToJson(ProductTypeEntity entity,
                                 ObjectNode gender,
                                 ObjectNode clothes,
                                 ObjectNode shoes,
                                 ArrayNode clothesArray,
                                 ArrayNode shoesArray) {
        log.debug("addObjectToJson() method is executed");
        if (!gender.hasNonNull("href"))
            gender.put("href", entity.getGender().getHref());
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode newItem = mapper.createObjectNode();
        if (entity.getClazz().getName().equals(Variable.CLOTHES)) {
            checkUniqueField(entity, clothes);
            newItem.put("name", entity.getName().getName());
            newItem.put("href", entity.getName().getHref());
            clothesArray.add(newItem);
        } else {
            checkUniqueField(entity, shoes);
            newItem.put("name", entity.getName().getName());
            newItem.put("href", entity.getName().getHref());
            shoesArray.add(newItem);
        }
    }
    private void checkUniqueField(ProductTypeEntity entity, ObjectNode clothes) {
        log.debug("checkUniqueField() method is executed");
        if (!clothes.hasNonNull("href")) {
            clothes.put("href", entity.getClazz().getHref());
        }
    }
    private void buildJson(ArrayNode baseJson,
                           ArrayNode classList,
                           ObjectNode clothes,
                           ObjectNode shoes,
                           ArrayNode itemsShoes,
                           ArrayNode itemsClothes,
                           ObjectNode genderObject) {
        log.debug("buildJson() method is executed");
        shoes.set("items", itemsShoes);
        clothes.set("items", itemsClothes);
        classList.add(shoes);
        classList.add(clothes);
        genderObject.set("items", classList);
        baseJson.add(genderObject);
    }
}
