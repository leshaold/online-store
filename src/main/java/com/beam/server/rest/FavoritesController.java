package com.beam.server.rest;

import com.beam.server.domain.JwtAuthentication;
import com.beam.server.model.*;
import com.beam.server.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/api/client")
@Api(tags = "/api/client")
@CrossOrigin
public class FavoritesController {

    private ObjectMapper mapper = new ObjectMapper();
    @Autowired
    private IFavoritesService favoritesService;
    @Autowired
    private IClientService clientService;
    @Autowired
    private IProductService productService;
    @Autowired
    private AuthService authService;

    @ApiOperation(value = "Добавление товара в избранное", notes = "Добавление товара в избранное")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученых данных"),
            @ApiResponse(code = 404, message = "Данные не найдены в БД"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    @PreAuthorize("hasAuthority('CLIENT')")
    @RequestMapping(value = "/favorites", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> addToFavorites(@RequestParam  long productId) {
        final JwtAuthentication authInfo = authService.getAuthInfo();
        long clientId = Long.valueOf(authInfo.getId());
        FavoritesEntity favoritesEntity = favoritesService.findByClientAndProduct(clientId, productId);
        if (favoritesEntity != null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        ClientEntity client;
        ProductEntity product;
        try {
            Optional<ClientEntity> clientEntityOptional = clientService.getClientById(clientId);
            client = clientEntityOptional.get();
            Optional<ProductEntity> productEntityOptional = productService.findById(productId);
            product = productEntityOptional.get();
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        favoritesEntity = new FavoritesEntity();
        favoritesEntity.setClient(client);
        favoritesEntity.setProduct(product);
        favoritesService.addProduct(favoritesEntity);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление товара из избранного", notes = "Удаление товара из избранного")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученых данных"),
            @ApiResponse(code = 404, message = "Данные не найдены в БД"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    @PreAuthorize("hasAuthority('CLIENT')")
    @RequestMapping(value = "/favorites", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayNode> deleteFromFavorites (@RequestBody List<Long> productList) {
        final JwtAuthentication authInfo = authService.getAuthInfo();
        long clientId = Long.valueOf(authInfo.getId());
        for (long productId : productList) {
            if (productId < 1)
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            favoritesService.deleteByClientAndProduct(clientId, productId);
        }

        return getFavorites(clientId);
    }

    @ApiOperation(value = "Получение товаров в избранном", notes = "Получение товаров в избранном")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Запрос выполнен успешно")
    })
    @PreAuthorize("hasAuthority('CLIENT')")
    @RequestMapping(value = "/favorites", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayNode> getFavorites() {
        final JwtAuthentication authInfo = authService.getAuthInfo();
        long clientId = Long.valueOf(authInfo.getId());

        return getFavorites(clientId);
    }

    private ResponseEntity<ArrayNode> getFavorites(long clientId) {
        List<FavoritesEntity> favorites = favoritesService.findAllByClient(clientId);
        ArrayNode products = mapper.createArrayNode();
        for (FavoritesEntity entity : favorites) {
            ObjectNode item = mapper.createObjectNode();
            item.put("id", entity.getProduct().getId());
            item.put("name", entity.getProduct().getName());
            item.put("img", Utils.getImgBase64(entity.getProduct().getImg()));
            ObjectNode price = mapper.createObjectNode();
            price.put("value", entity.getProduct().getPrice());
            price.put("currency", "₽");
            item.put("price", entity.getProduct().getPrice());
            products.add(item);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }
}
