package com.beam.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;

@Getter
@Setter
@Entity
@Table(name = "orders", schema = "store")
public class OrderEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "date", nullable = false)
    private Date date;

    @ManyToOne
    @JoinColumn(name = "client", referencedColumnName = "id", nullable = false)
    private ClientEntity client;

    @ManyToOne
    @JoinColumn(name = "delivery", referencedColumnName = "id", nullable = false)
    private DeliveryEntity delivery;

    @ManyToOne
    @JoinColumn(name = "employee", referencedColumnName = "id")
    private ClientEntity employee;

    @ManyToOne
    @JoinColumn(name = "status", referencedColumnName = "id", nullable = false)
    private StatusEntity status;

    @OneToMany(mappedBy = "orderEntity")
    private Collection<OrderBodyEntity> orderBodies;
}
