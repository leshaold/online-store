package com.beam.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "product_variants", schema = "store")
public class ProductVariantsEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "qty")
    private int qty;

    @Column(name = "img")
    private byte[] img;

    @ManyToOne
    @JoinColumn(name = "product", referencedColumnName = "id")
    private ProductEntity product;

    @ManyToOne
    @JoinColumn(name = "color", referencedColumnName = "id")
    private ColorEntity color;

    @ManyToOne
    @JoinColumn(name = "size", referencedColumnName = "id")
    private SizeEntity size;
}

