package com.beam.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "size", schema = "store")
public class SizeEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "size", nullable = false)
    private String size;

    @ManyToOne
    @JoinColumn(name = "gender", referencedColumnName = "id", nullable = false)
    private GenderEntity gender;

    @ManyToOne
    @JoinColumn(name = "class", referencedColumnName = "id", nullable = false)
    private ProductClassEntity clazz;
}
