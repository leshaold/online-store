package com.beam.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "product_type", schema = "store")
public class ProductTypeEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private long id;

    @ManyToOne
    @JoinColumn(name = "name", referencedColumnName = "id")
    private CategoriesEntity name;

    @Column(nullable = false)
    private byte[] img;

    @ManyToOne
    @JoinColumn(name = "gender", referencedColumnName = "id", nullable = false)
    private GenderEntity gender;

    @ManyToOne
    @JoinColumn(name = "class", referencedColumnName = "id", nullable = false)
    private ProductClassEntity clazz;
}
