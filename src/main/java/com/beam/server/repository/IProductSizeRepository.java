package com.beam.server.repository;

import com.beam.server.model.SizeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IProductSizeRepository  extends JpaRepository<SizeEntity, Integer> {
    List<SizeEntity> findAllByGender_HrefAndClazz_Href(String gender, String clazz);
    List<SizeEntity> findAllByGender_Href(String gender);
}
