package com.beam.server.repository;

import com.beam.server.model.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;

public interface IProductRepository extends JpaRepository<ProductEntity, Long>,
        JpaSpecificationExecutor<ProductEntity> {
    public final static String SEARCH = "SELECT DISTINCT p.id, p.name, p.type, p.price, p.img\n" +
            "FROM store.\"product_variants\" AS pv\n" +
            "JOIN store.\"product\" AS p ON pv.product = p.id\n" +
            "JOIN store.\"product_type\" ON type = store.\"product_type\".id\n" +
            "JOIN store.\"color\" ON color = store.\"color\".id\n" +
            "JOIN store.\"gender\" ON gender = store.\"gender\".id\n" +
            "WHERE CONCAT(store.\"color\".name, \' \', store.\"gender\".name, \' \', p.name) LIKE %:search% LIMIT 5";

    @Query(value = SEARCH, nativeQuery = true)
    List<ProductEntity> searchByWord(@Param("search") String word);
}
