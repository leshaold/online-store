package com.beam.server.repository;

import com.beam.server.model.ProductEntity;
import com.beam.server.model.ProductVariantsEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface IProductVariantRepository extends JpaRepository<ProductVariantsEntity, Long>,
        JpaSpecificationExecutor<ProductVariantsEntity>{
}
