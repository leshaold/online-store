package com.beam.server.service;

import com.beam.server.model.StatusEntity;
import com.beam.server.repository.IStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StatusService implements IStatusService {

    @Autowired
    IStatusRepository statusRepository;

    @Override
    public List<StatusEntity> findAll() {
        return statusRepository.findAll();
    }

    @Override
    public Optional<StatusEntity> findById(long id) {
        return statusRepository.findById(id);
    }
}
