package com.beam.server.service;

import com.beam.server.model.ProductTypeEntity;

import java.util.List;
import java.util.Optional;

public interface IProductTypeService {
    List<ProductTypeEntity> getAllProductTypeByGenderAndClazz(String gender, String clazz);

    List<ProductTypeEntity> getAllProductTypeByGender(String gender);
    List<ProductTypeEntity> getAllProductType();
    long getCountProductType();
    Optional getProductTypeEntityById(long id);
}
