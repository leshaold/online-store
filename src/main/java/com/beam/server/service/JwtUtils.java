package com.beam.server.service;

import com.beam.server.domain.JwtAuthentication;
import com.beam.server.domain.Role;
import io.jsonwebtoken.Claims;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JwtUtils {

    private static final String CLIENT = "CLIENT";
    public static JwtAuthentication generate(Claims claims) {
        log.info("generate() method is executed");
        final JwtAuthentication jwtInfoToken = new JwtAuthentication();
        jwtInfoToken.setRoles(getRoles(claims));
        jwtInfoToken.setFullName(claims.get("fullName", String.class));
        jwtInfoToken.setEmail(claims.getSubject());
        jwtInfoToken.setId(claims.get("id", String.class));
        return jwtInfoToken;
    }
    private static Set<Role> getRoles(Claims claims) {
        log.info("getRoles() method is executed");
        final Integer role = claims.get("roles", Integer.class);
        final List<String> rolesStr = new ArrayList<>();
        if (role == 1)
            rolesStr.add(CLIENT);
        return rolesStr.stream()
                .map(Role::valueOf)
                .collect(Collectors.toSet());
    }
}
