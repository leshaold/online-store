package com.beam.server.service;

import com.beam.server.model.SizeEntity;
import com.beam.server.repository.IProductSizeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductSizeService implements IProductSizeService {
    @Autowired
    IProductSizeRepository productSizeRepository;

    @Override
    public List<SizeEntity> getAllProductSizeByGenderAndClazz(String gender, String clazz) {
        return productSizeRepository.findAllByGender_HrefAndClazz_Href(gender, clazz);
    }

    @Override
    public List<SizeEntity> getAllProductSizeByGender(String gender) {
        return productSizeRepository.findAllByGender_Href(gender);
    }
}
