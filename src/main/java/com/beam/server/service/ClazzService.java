package com.beam.server.service;

import com.beam.server.model.ProductClassEntity;
import com.beam.server.repository.IClazzRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClazzService implements IClazzService {

    @Autowired
    private IClazzRepository clazzRepository;

    @Override
    public List<ProductClassEntity> findAll() {
        return clazzRepository.findAll();
    }
}
