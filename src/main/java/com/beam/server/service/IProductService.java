package com.beam.server.service;

import com.beam.server.model.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface IProductService {
    Page<ProductEntity> findAll (Pageable pageable);
    Optional<ProductEntity> findById (long id);
    List<ProductEntity> searchByWord (String word);
    Page<ProductEntity> findAll(Specification<ProductEntity> specification, Pageable pageable);
    void save (ProductEntity product);
}
