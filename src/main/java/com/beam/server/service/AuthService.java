package com.beam.server.service;

import com.beam.server.domain.JwtAuthentication;
import com.beam.server.domain.JwtRequest;
import com.beam.server.domain.JwtResponse;
import com.beam.server.model.ClientEntity;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.security.auth.message.AuthException;
import java.util.HashMap;
import java.util.Map;
@Slf4j
@Service
@RequiredArgsConstructor
public class AuthService {
    private final ClientService clientService;
    private final CartService cartService;
    private final FavoritesService favoritesService;
    private final Map<String, String> refreshStorage = new HashMap<>();
    private final JwtProvider jwtProvider;

    public JwtResponse login(@NonNull JwtRequest authRequest) throws AuthException {
        log.info("login() method is executed");
        final ClientEntity client = clientService.getClientByEmail(authRequest.getEmail());
        if (client == null)
            throw new AuthException("User not found");

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (encoder.matches(authRequest.getPassword(), client.getPassword())) {
            final String accessToken = jwtProvider.generateAccessToken(client,
                    cartService.countCartByClient(client.getId()),
                    favoritesService.countFavoritesByClient(client.getId()));
            final String refreshToken = jwtProvider.generateRefreshToken(client);
            refreshStorage.put(client.getEmail(), refreshToken);
            return new JwtResponse(accessToken, refreshToken);
        } else {
            throw new AuthException("Password incorrect");
        }
    }

    public JwtResponse getAccessToken(@NonNull String refreshToken) throws AuthException {
        log.info("getAccessToken() method is executed");
        if (jwtProvider.validateRefreshToken(refreshToken)) {
            final Claims claims = jwtProvider.getRefreshClaims(refreshToken);
            final String email = claims.getSubject();
            final String saveRefreshToken = refreshStorage.get(email);
            if (saveRefreshToken != null && saveRefreshToken.equals(refreshToken)) {
                final ClientEntity client = clientService.getClientByEmail(email);
                if (client == null)
                    throw new AuthException("User not found");
                final String accessToken = jwtProvider.generateAccessToken(client,
                        cartService.countCartByClient(client.getId()),
                        favoritesService.countFavoritesByClient(client.getId()));
                return new JwtResponse(accessToken, null);
            }
        }
        return new JwtResponse(null, null);
    }

    public JwtResponse refresh(@NonNull String refreshToken) throws AuthException {
        log.info("refresh() method is executed");
        if (jwtProvider.validateRefreshToken(refreshToken)) {
            final Claims claims = jwtProvider.getRefreshClaims(refreshToken);
            final String email = claims.getSubject();
            final String saveRefreshToken = refreshStorage.get(email);
            if (saveRefreshToken != null && saveRefreshToken.equals(refreshToken)) {
                final ClientEntity client = clientService.getClientByEmail(email);
                if (client == null)
                    throw new AuthException("User not found");
                final String accessToken = jwtProvider.generateAccessToken(client,
                        cartService.countCartByClient(client.getId()),
                        favoritesService.countFavoritesByClient(client.getId()));
                final String newRefreshToken = jwtProvider.generateRefreshToken(client);
                refreshStorage.put(client.getEmail(), newRefreshToken);
                return new JwtResponse(accessToken, newRefreshToken);
            }
        }
        throw new AuthException("JWT token invalid");
    }
    public JwtAuthentication getAuthInfo() {
        return (JwtAuthentication) SecurityContextHolder.getContext().getAuthentication();
    }

}
