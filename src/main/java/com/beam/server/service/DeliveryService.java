package com.beam.server.service;

import com.beam.server.model.DeliveryEntity;
import com.beam.server.repository.IDeliveryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DeliveryService implements IDeliveryService {

    @Autowired
    IDeliveryRepository deliveryRepository;

    @Override
    public Optional<DeliveryEntity> findById(long id) {
        return deliveryRepository.findById(id);
    }
    @Override
    public List<DeliveryEntity> findAll() {
        return deliveryRepository.findAll();
    }
}
