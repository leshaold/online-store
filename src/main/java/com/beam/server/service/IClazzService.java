package com.beam.server.service;

import com.beam.server.model.ProductClassEntity;

import java.util.List;

public interface IClazzService {
    List<ProductClassEntity> findAll();
}
