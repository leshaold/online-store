package com.beam.server.service;

import com.beam.server.model.CartEntity;

import java.util.List;

public interface ICartService {
    void addProductToCart (CartEntity cartEntity);
    List<CartEntity> findAllByIdClient (long id);
    CartEntity findByClientAndProduct (long clientId, long productVariantsId);
    void deleteByClientAndProduct (long clientId, long productVariantId);
    void updateCart(CartEntity cartEntity);
    long countCartByClient(long clientId);
}
