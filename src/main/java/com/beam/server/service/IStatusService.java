package com.beam.server.service;

import com.beam.server.model.StatusEntity;

import java.util.List;
import java.util.Optional;

public interface IStatusService {

    List<StatusEntity> findAll();

    Optional<StatusEntity> findById(long id);
}
