package com.beam.server.domain;

public final class Variable {
    public static final String FOR_MAN = "Мужчины";
    public static final String FOR_WOMAN = "Женщины";
    public static final String FOR_BOY = "Мальчики";
    public static final String FOR_GIRL = "Девочки";
    public static final String CLOTHES = "Одежда";
    public static final String SHOES = "Обувь";
}
