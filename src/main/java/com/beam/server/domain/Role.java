package com.beam.server.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@RequiredArgsConstructor
public enum Role implements GrantedAuthority {

    CLIENT ("CLIENT"),
    ADMIN ("ADMIN");
    private final String vale;

    @Override
    public String getAuthority() {
        return vale;
    }
}

